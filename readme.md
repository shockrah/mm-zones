# [Download - BHOP ZONES](https://gitlab.com/shockrah/mm-zones/-/jobs/artifacts/master/download?job=bhop)

# [Download - SURF ZONES](https://gitlab.com/shockrah/mm-zones/-/jobs/artifacts/master/download?job=surf)

# [Download - BHOP+SURF ZONES](https://gitlab.com/shockrah/mm-zones/-/jobs/artifacts/master/download?job=all)

# Momentum-Mod Zones

## Installing

Take the all the zone files themselves from the zip's `bhop` or `surf` folder and paste them into: 

```
\Path\To\SteamLibrary\steamapps\common\Momentum Mod\momentum\maps
```

## [How do I contribute?](https://gitlab.com/shockrah/mm-zones/blob/master/CONTRIBUTING.md)

tl;dr send me things through one of the following methods below:

* Discord: shockrah#2647
* Email: alerah@protonmail.com
* Merge request

Check the [contributing guide](https://gitlab.com/shockrah/mm-zones/blob/master/CONTRIBUTING.md) for more info like how to make zones that make sense(in my opnion).

:\^)

